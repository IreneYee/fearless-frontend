import React, { useEffect } from 'react';

function LocationForm( ) {
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        const option = document.createElement('option');
        option.value = state.abbreviation;
        option.innerHTML = state.name;
        selectTag.appendChild(option);
      }
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <p>A location form</p>
  )
}

export default LocationForm;