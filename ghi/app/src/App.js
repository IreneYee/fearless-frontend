import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from "./AttendeesList";
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import MainPage from "./MainPage";
import PresentationForm from "./PresentationForm";



function App(props) {
  // if (props.attendees === undefined) {
  //   return null;
  // }

  return (
    <BrowserRouter>
      <Nav />
      {/* <div className="container"> */}
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm/>} />
          </Route>
            <Route path="attendees">
              <Route index element={<AttendeesList />} />
              <Route path="new" element={<AttendConferenceForm/>} />
            </Route>
            <Route path="presentations">
              <Route path="new" element={<PresentationForm />} />
            </Route>
          </Routes>

    </BrowserRouter>
  );
}

export default App;

      {/* <LocationForm />   
      <ConferenceForm /> 
      <AttendConferenceForm />  */}
      {/* <AttendeesList attendees={props.attendees} /> */}